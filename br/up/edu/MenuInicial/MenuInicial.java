import java.util.Scanner;

public class MenuInicial {
    public static void executar(){
        int opcao;
    }
    public static int mostrarMenu() {
        Scanner leitor = new Scanner(System.in);
        int opcao;
        do {
            System.out.println("### MENU DE EXERCÍCIOS ###");
            System.out.println("1. Exercício 1: Calcular Média do Aluno");
            System.out.println("2. Exercício 2: Calcular Consumo Médio de Automóvel");
            System.out.println("3. Exercício 3: Calcular Salário de Vendedor com Comissão");
            System.out.println("4. Exercício 4: Conversão de Dólar para Real");
            System.out.println("5. Exercício 5: Calcular Valor das Prestações");
            System.out.println("6. Exercício 6: Calcular Valor de Venda de Produto");
            System.out.println("7. Exercício 7: Calcular Custo ao Consumidor de um Carro");
            System.out.println("8. Exercício 8: Verificar Situação do Aluno");
            System.out.println("9. Exercício 9: Contar Números no Intervalo");
            System.out.println("10. Exercício 10: Verificar Maioridade de Pessoas");
            System.out.println("11. Exercício 11: Informar Sexo das Pessoas");
            System.out.println("12. Exercício 12: Calcular Desconto em Concessionária de Veículos");
            System.out.println("13. Exercício 13: Verificar Aptidão para Serviço Militar Obrigatório");
            System.out.println("14. Exercício 14: Verificar Lucro ou Prejuízo em Produtos");
            System.out.println("15. Exercício 15: Calcular Desconto em Concessionária de Veículos (2)");
            System.out.println("16. Exercício 16: Calcular Reajuste Salarial de Funcionários");
            System.out.println("17. Exercício 17: Calcular Novo Salário e Aumento da Folha");
            System.out.println("18. Exercício 18: Calcular Abono Salarial por Sexo e Idade");
            System.out.println("19. Exercício 19: Verificar Tipo de Triângulo");
            System.out.println("20. Exercício 20: Calcular Salário de Professor");
            System.out.println("21. Exercício 21: Classificar Nadador");
            System.out.println("22. Exercício 22: Calcular Valor da Conta de Luz");
            System.out.println("23. Exercício 23: Calcular Peso Ideal");
            System.out.println("24. Exercício 24: Calcular Nota Final do Estudante");
            System.out.println("25. Exercício 25: Determinar Categoria de Seguro");
            System.out.println("0. Sair");

            System.out.print("Escolha uma opção: ");
            opcao = leitor.nextInt();

            switch (opcao) {
                case 1:
                Exercicio01.executar();
                    break;
                case 2:
                ExercicIo02.executar();
                    break;
                case 3:
                Exercicio03.executar();
                    break;
                case 4:
                Exercicio04.executar();
                    break;
                case 5:
                Exercicio05.executar();
                    break;
                case 6:
                Exercicio06.executar();
                    break;
                case 7:
                Exercicio07.executar();
                    break;
                case 8:
                Exercicio08.executar();
                    break;
                case 9:
                Exercicio09.executar();
                    break;
                case 10:
                Exercicio10.executar();
                    break;
                case 11:
                Exercicio11.executar();
                    break;
                case 12:
                Exercicio12.executar();
                    break;
                case 13:
                Exercicio13.executar();
                    break;
                case 14:
                Exercicio14.executar();
                    break;
                case 15:
                Exercicio15.executar();
                    break;
                case 16:
                Exercicio16.executar();
                    break;
                case 17:
                Exercicio17.executar();
                    break;
                case 18:
                Exercicio18.executar();
                    break;
                case 19:
                Exercicio19.executar();
                    break;
                case 20:
                Exercicio20.executar();
                    break;
                case 21:
                Exercicio21.executar();
                    break;
                case 22:
                Exercicio22.executar();
                    break;
                case 23:
                Exercicio23.executar();
                    break;
                case 24:
                Exercicio24.executar();
                    break;
                case 25:
                Exercicio25.executar();
                    break;
                case 26:
                Exercicio26.executar();
                    break;
                case 0:
                    System.out.println("Encerrando o programa...");
                    break;
                default:
                    System.out.println("Opção inválida! Tente novamente.");
            }
        } while (opcao != 0);

        leitor.close();
    }
}
