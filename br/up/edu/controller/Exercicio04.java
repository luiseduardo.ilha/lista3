package controller;
import java.util.Scanner;

public class Exercicio04 {
    public static void executar() {
        double cotacao = obterCotacao();
        double valor = obterValor();

        double valorConvertido = converterDolarParaReal(cotacao, valor);

        System.out.println("O valor em reais é: R$" + valorConvertido);
    }

    public static double obterCotacao() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite a cotação atual do dólar: ");
        double vDolar = leitor.nextDouble();
        return vDolar;
    }

    public static double obterValor() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o valor em dólar: ");
        double QuantDolar = leitor.nextDouble();
        return QuantDolar;
    }

    public static double converterDolarParaReal(double cotacao, double valor) {
        return cotacao * valor;
    }
}
