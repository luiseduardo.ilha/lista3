package controller;
public class Exercicio13 {
    public String verificarAptidao(String nome, char sexo, int idade, boolean saude) {
        EX13VerificacaoAptidao.VerificarAptidao.Pessoa pessoa = new EX13VerificacaoAptidao().new VerificarAptidao().new Pessoa(nome, sexo, idade, saude);
        return pessoa.verificarAptidao();
    }
}
