package controller;
import java.util.Scanner;
public class Exercicio24 {
    public static double getNota1() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe o valor tirado no laboratório: ");
        double notaLab = leitor.nextDouble();

        return notaLab;
    }
    public static double getNota2() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe o valor tirado na avaliação semestral: ");
        double notaSemestral = leitor.nextDouble();

        return notaSemestral;
    }
    public static double getNota3() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe o valor tirado na prova final: ");
        double notaFinal = leitor.nextDouble();

        return notaFinal;
    }
}
