package controller;
import java.util.Scanner;

public class Exercicio11 {
    public String verificarGenero(String nome, char sexo) {
        EX11VerificacaoGenero.VerificarGenero.Pessoa pessoa = new EX11VerificacaoGenero().new VerificarGenero().criarPessoa(nome, sexo);
        return pessoa.getSexo();
    }
}
