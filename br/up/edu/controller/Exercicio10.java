package controller;
import java.util.Scanner;

public class Exercicio10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Quantas pessoas serão verificadas? ");
        int quantidadePessoas = scanner.nextInt();

        for (int i = 1; i <= quantidadePessoas; i++) {
            String classificacao = EX10ClassificacaoIdade.VerificarMaioridade.Pessoa.obterClassificacao();
            System.out.println("Pessoa " + i + ": " + classificacao);
        }

        scanner.close();
    }
}