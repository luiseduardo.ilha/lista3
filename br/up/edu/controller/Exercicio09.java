package controller;
import java.util.Scanner;

public class Exercicio09 {
    public static int ObterNumeros() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite a quantidade de números a serem lidos: ");
        int quantidadeNumeros = leitor.nextInt();

        EX09ContadorIntervalo contador = new EX09ContadorIntervalo(quantidadeNumeros);
        contador.lerNumeros();

        System.out.print("Digite o limite inferior do intervalo: ");
        int limiteInferior = leitor.nextInt();
        System.out.print("Digite o limite superior do intervalo: ");
        int limiteSuperior = leitor.nextInt();

        int quantidadeNoIntervalo = contador.contarNumerosNoIntervalo(limiteInferior, limiteSuperior);

        leitor.close();
        return quantidadeNoIntervalo;
    }
}
