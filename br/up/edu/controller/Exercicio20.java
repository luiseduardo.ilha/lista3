package controller;
import java.util.Scanner;

public class Exercicio20 {
    public static void main(String[] args) {
        int nivel = getNivelProfessor();
        int horasAula = getHorasAula();
        
        EX20SalarioProfessor professor = new EX20SalarioProfessor(nivel);
        double salario = professor.calcularSalario(horasAula);
        
        System.out.println("O salário do professor é: R$" + salario);
    }

    public static int getNivelProfessor() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Digite o nível do professor (1, 2 ou 3): ");
        return leitor.nextInt();
    }

    public static int getHorasAula() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Digite o número de horas/aula dadas pelo professor: ");
        return leitor.nextInt();
    }
}
