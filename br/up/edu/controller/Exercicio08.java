package controller;
import java.util.Scanner;
public class Exercicio08{
    public static String obterNomeAluno() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o nome do aluno: ");
        String nome = leitor.nextLine();
        return nome;
    }
    public static double obterNota1(){
        Scanner leitor = new Scanner(System.in);

        System.out.println("Digite a nota 1 do aluno: ");
        double n1 = leitor.nextDouble();

        return n1;
    }
    public static double obterNota2(){
        Scanner leitor = new Scanner(System.in);

        System.out.println("Digite a nota 2 do aluno: ");
        double n2 = leitor.nextDouble();

        return n2;
    }
    public static double obterNota3(){
        Scanner leitor = new Scanner(System.in);

        System.out.println("Digite a nota 3 do aluno: ");
        double n3 = leitor.nextDouble();

        return n3;
    }
}