package controller;
import java.util.Scanner;

public class Exercicio22 {
    public static int obterTipoCliente() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Informe o tipo de cliente: ");
        int tipoCliente = leitor.nextInt();
        return tipoCliente;
    }

    public static double obterConsumo() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Informe o quanto foi consumido: ");
        double consumo = leitor.nextDouble();
        return consumo;
    }
}
