package controller;
import java.util.Scanner;
public class Exercicio26 {
    public static String nome(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe seu nome: ");
        String nome = leitor.nextLine();

        return nome;
    }
    public static int idade(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe sua idade: ");
        int idade = leitor.nextInt();

        return idade;
    }
    public static String ocupacao(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe sua ocupação (Baixo, médio ou alto): ");
        String ocupacao = leitor.nextLine().toLowerCase();

        return ocupacao;
    }
}
