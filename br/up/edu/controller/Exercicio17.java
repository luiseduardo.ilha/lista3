package controller;
import java.util.Scanner;

public class Exercicio17 {
    public static String getNome() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o nome do funcionário: ");
        String nome = leitor.nextLine();
        return nome;
    }
    public static double getSalario() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Digite o salário do funcionário: ");
        double salario = leitor.nextDouble();
        return salario;
    }
    public static double getSalMin() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Digite o valor do salário mínimo: ");
        double salarioMinimo = leitor.nextDouble();
        return salarioMinimo;
    }
}
