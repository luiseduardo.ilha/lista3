package controller;
import java.util.Scanner;

public class Exercicio15 {
    public double calcularDesconto(String combustivel, double valorVeiculo) {
        EX15DescontoConcessionaria.CalcularDesconto.Veiculo veiculo = new EX15DescontoConcessionaria().new CalcularDesconto().new Veiculo(combustivel, valorVeiculo);
        return veiculo.calcularDesconto();
    }
}
