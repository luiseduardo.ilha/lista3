package controller;
import java.util.Scanner;

public class Exercicio06 {
    public static void executar() {
        double custo = obterCusto();
        double acrescimo = obterAcrescimo();

        double valorVenda = calcularValorVenda(custo, acrescimo);

        System.out.println("O valor de venda do produto é: R$" + valorVenda);
    }

    public static double obterCusto() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o custo do produto: ");
        double cProduto = leitor.nextDouble();
        return cProduto;
    }

    public static double obterAcrescimo() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o percentual de acréscimo (%): ");
        double acrescimo = leitor.nextDouble();
        return acrescimo;
    }

    public static double calcularValorVenda(double custo, double acrescimo) {
        return custo * (1 + acrescimo / 100);
    }
}
