package controller;
import java.util.Scanner;

public class Exercicio03 {
    public static void executar() {
        String nome = obterNomeVendedor();
        double salFix = obterSalarioFixo();
        double vendas = obterTotalVendido();

        double salarioTotal = calcularSalarioTotal(salFix, vendas);

        System.out.println("O vendedor " + nome + " deve receber um salário total de R$" + salarioTotal);
    }

    public static String obterNomeVendedor() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o nome do vendedor: ");
        String nome = leitor.nextLine();
        return nome;
    }

    public static double obterSalarioFixo() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o salário fixo do vendedor: ");
        double salFix = leitor.nextDouble();
        return salFix;
    }

    public static double obterTotalVendido() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o total vendido pelo vendedor: ");
        double vendas = leitor.nextDouble();
        return vendas;
    }

    public static double calcularSalarioTotal(double salFix, double vendas) {
        return salFix + (vendas * 0.03); // Assumindo uma comissão de 3%
    }
}
