package controller;
import java.util.Scanner;

public class Exercicio14 {
    public static double ObterPrecoCusto() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o custo do produto: ");
        double PrecoCusto = leitor.nextDouble();

        return PrecoCusto;
    }
    public static double ObterPrecoVenda(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o valor que o produto está sendo vendido: ");
        double PrecoVenda = leitor.nextDouble();

        return PrecoVenda;
    }
}