package controller;
import java.util.Scanner;

public class Exercicio07 {
    public static void executar() {
        double custoProduto = obterCustoProduto();
    }

    public static double obterCustoProduto() {
        Scanner leitor = new Scanner(System.in);
        System.out.print("Digite o custo do produto: ");
        double vCusto = leitor.nextDouble();
        return vCusto;
    }
}
