package controller;
import java.util.Scanner;

public class Exercicio23 {
    public static String getNome(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe seu nome: ");
        String nome = leitor.nextLine();

        return nome;
    }
    public static char getSexo(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe seu sexo: ");
        char sexo = leitor.next().charAt(0);

        return sexo;
    }
    public static double getAltura(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe sua altura: ");
        double altura = leitor.nextDouble();

        return altura;
    }
    public static int getIdade(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Informe sua idade: ");
        int idade = leitor.nextInt();

        return idade;
    }
}