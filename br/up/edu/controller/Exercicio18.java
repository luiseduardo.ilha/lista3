package controller;
import java.util.Scanner;
public class Exercicio18 {
    public static String getNome() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o nome do funcionário: ");
        String nome = leitor.nextLine();

        return nome;
    }

    public static int getIdade(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite a idade do funcionário: ");
        int idade = leitor.nextInt();

        return idade;
    }
    public static char getSexo() {
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o sexo do funcionário (M/F): ");
        char sexo = leitor.next().charAt(0);

        return sexo;
    }
    public static double getSalario(){
        Scanner leitor = new Scanner(System.in);

        System.out.print("Digite o salário do funcionário: ");
        double salario = leitor.nextDouble();
        
        return salario;
    }
}
