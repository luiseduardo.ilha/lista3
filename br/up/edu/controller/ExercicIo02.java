package controller;
import java.util.Scanner;

public class ExercicIo02 {
    public static void executar() {
        double distancia = obterDistancia();
        double consumo = obterConsumo();

        double consumoMedio = calcularConsumoMedio(distancia, consumo);

        System.out.println("O consumo médio do automóvel é: " + consumoMedio + " km/L");
    }

    public static double obterDistancia() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite a distância percorrida (em km): ");
        double distancia = leitor.nextDouble();
        return distancia;
    }

    public static double obterConsumo() {
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o total de combustível consumido (em litros): ");
        double totalGasto = leitor.nextDouble();
        return totalGasto;
    }

    public static double calcularConsumoMedio(double distancia, double consumo) {
        return distancia / consumo;
    }
}
