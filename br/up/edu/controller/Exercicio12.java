package controller;
import java.util.Scanner;

public class Exercicio12 {
    public double calcularDesconto(int ano, double valorVeiculo) {
        EX12DescontoConcessionaria.CalcularDesconto.Veiculo veiculo = new EX12DescontoConcessionaria().new CalcularDesconto().new Veiculo(ano);
        return veiculo.calcularDesconto(valorVeiculo);
    }
}
