package view;
public class EX26Seguro {
    private String nome;
    private int idade;
    private String ocupacao;

    public EX26Seguro(String nome, int idade, String ocupacao) {
        this.nome = nome;
        this.idade = idade;
        this.ocupacao = ocupacao;
    }

    public String categorizarSegurado() {
        if (idade >= 17 && idade <= 20) {
            switch (ocupacao) {
                case "baixo":
                    return "Categoria 1";
                case "medio":
                    return "Categoria 2";
                case "alto":
                    return "Categoria 3";
            }
        } else if (idade >= 21 && idade <= 24) {
            switch (ocupacao) {
                case "baixo":
                    return "Categoria 2";
                case "medio":
                    return "Categoria 3";
                case "alto":
                    return "Categoria 4";
            }
        } else if (idade >= 25 && idade <= 34) {
            switch (ocupacao) {
                case "baixo":
                    return "Categoria 3";
                case "medio":
                    return "Categoria 4";
                case "alto":
                    return "Categoria 5";
            }
        } else if (idade >= 35 && idade <= 64) {
            switch (ocupacao) {
                case "baixo":
                    return "Categoria 4";
                case "medio":
                    return "Categoria 5";
                case "alto":
                    return "Categoria 6";
            }
        } else if (idade >= 65 && idade <= 70) {
            switch (ocupacao) {
                case "baixo":
                    return "Categoria 7";
                case "medio":
                    return "Categoria 8";
                case "alto":
                    return "Categoria 9";
            }
        }
        return "Não se enquadra em nenhuma categoria.";
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public String getOcupacao() {
        return ocupacao;
    }
}
