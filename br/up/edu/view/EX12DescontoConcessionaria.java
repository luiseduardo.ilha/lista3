package view;
public class EX12DescontoConcessionaria {
    public class CalcularDesconto {
        public class Veiculo {
            private int ano;

            public Veiculo(int ano) {
                this.ano = ano;
            }

            public double calcularDesconto(double valorVeiculo) {
                double desconto;
                if (ano <= 2000) {
                    desconto = valorVeiculo * 0.12;
                } else {
                    desconto = valorVeiculo * 0.07;
                }
                return desconto;
            }
        }
    }
}
