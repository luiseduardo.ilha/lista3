package view;
public class EX22ContaDeLuz {
    private String tipoCliente;
    private double valorKW;

    public EX22ContaDeLuz(String tipoCliente) {
        this.tipoCliente = tipoCliente;
        switch (tipoCliente) {
            case "1":
                valorKW = 0.60;
                break;
            case "2":
                valorKW = 0.48;
                break;
            case "3":
                valorKW = 1.29;
                break;
            default:
                valorKW = 0.0;
                break;
        }
    }

    public double calcularConta(double consumo) {
        return consumo * valorKW;
    }
}