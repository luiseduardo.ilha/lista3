package view;
public class EX24NotaAluno {
    private double notaLab;
    private double notaSemestral;
    private double notaExameFinal;

    public EX24NotaAluno(double notaLab, double notaSemestral, double notaExameFinal) {
        this.notaLab = notaLab;
        this.notaSemestral = notaSemestral;
        this.notaExameFinal = notaExameFinal;
    }

    public double calcularNotaFinal() {
        return (notaLab * 2 + notaSemestral * 3 + notaExameFinal * 5) / 10;
    }

    public String classificarAluno() {
        double notaFinal = calcularNotaFinal();
        if (notaFinal >= 8 && notaFinal <= 10) {
            return "A";
        } else if (notaFinal >= 7 && notaFinal < 8) {
            return "B";
        } else if (notaFinal >= 6 && notaFinal < 7) {
            return "C";
        } else if (notaFinal >= 5 && notaFinal < 6) {
            return "D";
        } else {
            return "R";
        }
    }
}