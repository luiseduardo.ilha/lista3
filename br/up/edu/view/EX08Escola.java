package view;
public class EX08Escola {
    public static class Aluno {
        private String nome;
        private int idade;
        private double n1;
        private double n2;
        private double n3;
        public Aluno(String nome, int idade){
            this.nome = nome;
            this.idade = idade;
            this.n1 = n1;
            this.n2 = n2;
            this.n3 = n3;
        }
        public String getNome(){
            return nome;
        }
        public int getIdade(){
            return idade;
        }
        public double getN1(){
            return n1;
        }
        public double getN2(){
            return n2;
        }
        public double getN3(){
            return n3;
        }
        double nota = (n1 + n2 + n3)/3;
        public void exibirDados(){
            System.out.println("Nome do aluno: " + nome);
            if (nota >=7) {
                System.out.println("Aprovado");
            }else if(nota<=5){
                System.out.println("Reprovado");
            }else{
                System.out.println("Em recuperação");
            }
        }
    }
}
