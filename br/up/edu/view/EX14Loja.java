package view;
public class EX14Loja {
    private double PrecoCusto;
    private double PrecoVenda;

    public EX14Loja(double PrecoCusto, double PrecoVenda){
        this.PrecoCusto = PrecoCusto;
        this.PrecoVenda = PrecoVenda;
    }
    public String CalcLucroPrejuizo() {
        double lucro = PrecoVenda - PrecoCusto;
        if (lucro > 0) {
            return "Lucro";
        } else if (lucro < 0) {
            return "Prejuízo";
        } else {
            return "Empate";
        }
    } 
}
