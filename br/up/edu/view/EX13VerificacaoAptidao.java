package view;
public class EX13VerificacaoAptidao {
    public class VerificarAptidao {
        public class Pessoa {
            private String nome;
            private char sexo;
            private int idade;
            private boolean saude;

            public Pessoa(String nome, char sexo, int idade, boolean saude) {
                this.nome = nome;
                this.sexo = sexo;
                this.idade = idade;
                this.saude = saude;
            }

            public String verificarAptidao() {
                if (idade >= 18 && saude) {
                    return "Apto para o serviço militar obrigatório";
                } else {
                    return "Inapto para o serviço militar obrigatório";
                }
            }
        }
    }
}
