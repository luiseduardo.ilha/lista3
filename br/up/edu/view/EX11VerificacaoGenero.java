package view;
import java.util.Scanner;

public class EX11VerificacaoGenero {
    public class VerificarGenero {
        public class Pessoa {
            private String nome;
            private char sexo;

            public Pessoa(String nome, char sexo) {
                this.nome = nome;
                this.sexo = sexo;
            }

            public String getNome() {
                return nome;
            }

            public String getSexo() {
                if (sexo == 'M' || sexo == 'm') {
                    return "Homem";
                } else if (sexo == 'F' || sexo == 'f') {
                    return "Mulher";
                } else {
                    return "Sexo não especificado";
                }
            }
        }

        public Pessoa criarPessoa(String nome, char sexo) {
            return new Pessoa(nome, sexo);
        }
    }
}
