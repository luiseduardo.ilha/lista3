package view;
public class EX20SalarioProfessor {
    private int nivel;
    private double valorHoraAula;

    public EX20SalarioProfessor(int nivel) {
        this.nivel = nivel;
        switch (nivel) {
            case 1:
                valorHoraAula = 12.0;
                break;
            case 2:
                valorHoraAula = 17.0;
                break;
            case 3:
                valorHoraAula = 25.0;
                break;
            default:
                valorHoraAula = 0.0;
                break;
        }
    }

    public double calcularSalario(int horasAula) {
        return horasAula * valorHoraAula;
    }
}
