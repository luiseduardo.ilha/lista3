package view;
public class EX16Empresa {
    public class CalcularDesconto {
        public class Veiculo {
            private double salario;
            private double SF;

            public Veiculo(double salario, double SF) {
                this.salario = salario;
                this.SF = SF;
            }

            public double calcularSalFinal() {
                SF = 0;
                double salMin = 1412;
                if (salario < salMin * 3) {
                    SF = salario + salario * 0.5;
                    System.out.println("Seu salário subiu 50%, agora esá no montante de: " + SF);
                }else if(salario >= salMin * 3 && salario <= salMin * 10){
                    SF = salario + salario * 0.2;
                    System.out.println("Seu salário subiu 20%, agora esá no montante de: " + SF);
                }else if(salario > salMin * 10 && salario <= salMin * 20){
                    SF = salario + salario * 0.15;
                    System.out.println("Seu salário subiu 15%, agora esá no montante de: " + SF);
                }else{
                    SF = salario + salario * 0.1;
                    System.out.println("Seu salário subiu 10%, agora esá no montante de: " + SF);
                }
                return SF;
            }
        }
    }
}
