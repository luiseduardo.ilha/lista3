package view;
public class EX17CalcReajuste {
    private String nome;
    private double salario;
    private double salarioMinimo;

    public EX17CalcReajuste(String nome, double salario, double salarioMinimo) {
        this.nome = nome;
        this.salario = salario;
        this.salarioMinimo = salarioMinimo;
    }

    public void calcularNovoSalario() {
        double reajuste;

        if (salario < salarioMinimo * 3) {
            reajuste = salario * 0.5;
        } else if (salario >= salarioMinimo * 3 && salario <= salarioMinimo * 10) {
            reajuste = salario * 0.2;
        } else if (salario > salarioMinimo * 10 && salario <= salarioMinimo * 20) {
            reajuste = salario * 0.15;
        } else {
            reajuste = salario * 0.1;
        }

        double novoSalario = salario + reajuste;

        System.out.println("Nome do funcionário: " + nome);
        System.out.println("Reajuste no salário: " + reajuste);
    }
}
