package view;
public class EX07Consecionaria {
    public class InserirvCarro {
        public class Valor {
            private double vCusto;
            public Valor(double vCusto){
                this.vCusto = vCusto;
            }
            public void exibirDados(){
                double imposto = 0.45;
                double taxa = 0.28;
                double vTotalCarro = vCusto + ((vCusto * imposto) * taxa);
                System.out.println("O valor do veículo somando os impostos mais taxa é de: " + vTotalCarro);
            }
        }
    }
}