package view;
public class EX02Carro {
    public class inserirCarro {
        public class Veiculo {
            private double distancia;
            private double totalGasto;
            public Veiculo(double distancia, double totalGasto){
                this.distancia = distancia;
                this.totalGasto = totalGasto;
            }
            public double getdistancia(){
                return distancia;
            }
            public double getTotalGasto(){
                return totalGasto;
            }
            public void exibirDados(){
                double consumo = distancia / totalGasto;
                System.out.println("Seu carro consomiu " + consumo + "KM/l");
            }
        }
    }
}
