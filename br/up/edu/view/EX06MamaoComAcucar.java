package view;
public class EX06MamaoComAcucar {
    public class inserirCompra {
        public class Valor {
            private double vCompra;
            private double qParcelas;
            public Valor(double vCompra, double qParcelas){
                this.vCompra = vCompra;
                this.qParcelas = qParcelas;
            }
            public double getvCompra(){
                return vCompra;
            }
            public double getParcelas(){
                return qParcelas;
            }
            public void exibirDados(){
                double vParcelas = vCompra / qParcelas;
                System.out.println("O valor parcelado em " + qParcelas + "prestações é de: " + vParcelas + "por mês");
            }
        }
    }
}
