package view;
public class EX04Conversor {
    public class inserirValor {
        public class Valor {
            private double vDolar;
            private double QuantDolar;
            public Valor(double vDolar, double QuantDolar){
                this.vDolar = vDolar;
                this.QuantDolar = QuantDolar;
            }
            public double getvDolar(){
                return vDolar;
            }
            public double getQuantDolar(){
                return QuantDolar;
            }
            public void exibirDados(){
                double real = vDolar * QuantDolar;
                System.out.println("A quantidade inserida de Dólar convertendo para real é: " + real);
            }
        }
    }
}
