package view;
public class EX09ContadorIntervalo {
    private int[] numeros;

    public EX09ContadorIntervalo(int quantidadeNumeros) {
        numeros = new int[quantidadeNumeros];
    }

    public void lerNumeros() {
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        for (int i = 0; i < numeros.length; i++) {
            System.out.print("Digite o número " + (i + 1) + ": ");
            numeros[i] = scanner.nextInt();
        }
        scanner.close();
    }

    public int contarNumerosNoIntervalo(int limiteInferior, int limiteSuperior) {
        int contador = 0;
        for (int numero : numeros) {
            if (numero >= limiteInferior && numero <= limiteSuperior) {
                contador++;
            }
        }
        return contador;
    }
}
