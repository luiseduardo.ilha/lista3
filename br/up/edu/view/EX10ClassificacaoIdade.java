package view;
public class EX10ClassificacaoIdade {
    public class VerificarMaioridade {
        public class Pessoa {
            private int idade;

            public Pessoa(int idade) {
                this.idade = idade;
            }

            public int getIdade() {
                return idade;
            }

            public String classificar() {
                if (idade >= 18) {
                    return "Maior de idade";
                } else {
                    return "Menor de idade";
                }
            }

            public static String obterClassificacao() {
                throw new UnsupportedOperationException("Unimplemented method 'obterClassificacao'");
            }
        }
    }
}