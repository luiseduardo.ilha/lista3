package view;
public class EX03Loja {
    public class Inserirvendedor {
        public class Vendedor {
            private String nome;
            private double salFix;
            private double vendas;
            public Vendedor(String nome, double salFix, double vendas){
                this.nome = nome;
                this.salFix = salFix;
                this.vendas = vendas;
            }
            public String getNome(){
                return nome;
            }
            public double getSalFix(){
                return salFix;
            }
            public double getVendas(){
                return vendas;
            }
            public void exibirDados(){
                double salFinal = salFix + vendas * 0.15;
                System.out.println("O vendedor(a) " + nome + "Que recebe um salário fixo de: " + salFix +
                "Com a comissão, seu salário total é de: " + salFinal);
            }
        }
    }
}
