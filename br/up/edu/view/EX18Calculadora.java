package view;
public class EX18Calculadora {
    private String nome;
    private int idade;
    private char sexo;
    private double salario;
    public EX18Calculadora(String nome, int idade, char sexo, double salario){
        this.nome = nome;
        this.idade = idade;
        this.sexo = sexo;
        this.salario = salario;
    }
    public double calcSalLiquido(){
        double abono = 0;
        if(sexo == 'M'){
            if(idade >=30){
                abono = 100;
            }else{
                abono = 50;
            }
        }else{
            if(idade>=30){
                abono = 200;
            }else{
                abono = 80;
            }
        }
        return salario + abono;
    }
    public String GetNome(){
        return nome;
    }
}
