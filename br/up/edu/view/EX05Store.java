package view;
public class EX05Store {
    public class inserirProduto {
        public class Valor {
            private double cProduto;
            private double acrescimo;
            public Valor(double cProduto, double acrescimo){
                this.cProduto = cProduto;
                this.acrescimo = acrescimo;
            }
            public double getcProduto(){
                return cProduto;
            }
            public double getAcrescimo(){
                return acrescimo;
            }
            public void exibirDados(){
                double vFinal = cProduto + cProduto * (acrescimo/100);
                System.out.println("O valor final do produto é de: " + vFinal);
            }
        }
    }
}